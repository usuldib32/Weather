Program pogodowy z zadania:
1. Dodaj do projektu plik weather-data.csv. Zawiera on informacje pogodowe z jednej stacji
meteorologicznej na przestrzeni prawie 70 lat (25 tysięcy rekordów).
2. Napisz program który pobierze od użytkownika datę i zwróci minimalną, średnią i
maksymalną temperaturę tego dnia.
3.Dodaj metodę która przyjmie 2 daty a następnie wyświetl jaka była najwyższa, najniższa i
średnia temperatura w tym okresie czasu.
4. Dodaj metodę która przyjmie liczbę całkowitą i zwróci liczbę dni w których średnia
temperatura była wyższa lub równa przekazanej.
5. Dodaj metodę która przeanalizuje i zwróci 2 informacje o datach oraz różnice temperatur:
1.Statystycznie który rok był najcieplejszy a który najchłodniejszy
2. Statystycznie który miesiąc był najcieplejszy a który najchłodniejszy
3. Statystycznie który kalendarzowy dzień był najcieplejszy a który najchłodniejszy