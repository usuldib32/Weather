package Weather;

public class WeatherData {
    private String date;
    private int minimumTecpC;
    private int maximuTecpC;
    private int meanTecpC;

    public WeatherData (String date, int minimumTecpC, int meanTecpC, int maximuTecpC) {
        this.date = date;
        this.minimumTecpC = minimumTecpC;
        this.meanTecpC = meanTecpC;
        this.maximuTecpC = maximuTecpC;
    }

    @Override
    public String toString() {
        return "data" + date + '\'' +
                ", min temp=" + minimumTecpC +
                ", max temp=" + maximuTecpC +
                ", mean temp=" + meanTecpC +
                "\n";
    }
}
