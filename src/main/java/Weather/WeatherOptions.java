package Weather;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.parseInt;

public class WeatherOptions {

    public static List<WeatherData> readFile(String path) throws IOException {
        List<WeatherData> weatherData = new ArrayList<>();
        String takeLine = "";
        File file = new File(path);
//        FileInputStream fis = new FileInputStream(file);
        FileReader fis = new FileReader(file);
        BufferedReader bfr = new BufferedReader(fis);
        while ((takeLine = bfr.readLine()) != null) {
            weatherData.add(dataToObj(takeLine));
        }

            return weatherData;
    }

    public static WeatherData dataToObj (String weatherDataString) {
        WeatherData day;
        try {
            String[] lineOfData = weatherDataString.split(",");
//        Integer.parseInt(lineOfData[1])),Integer.parseInt(lineOfData[2]), Integer.parseInt(lineOfData[3])
            day = new WeatherData(lineOfData[0], Integer.parseInt(lineOfData[1]), Integer.parseInt(lineOfData[2]), Integer.parseInt(lineOfData[3]));
        } catch (NumberFormatException e) {
            return null;
        }
        return day;
    }
}
